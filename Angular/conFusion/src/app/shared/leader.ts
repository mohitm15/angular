export class Leader {
  id: string;
  name: string;
  image: string; //URL
  designation: string;
  abbr: string;
  featured: boolean;
  description: string;
}
