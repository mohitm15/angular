**HOW TO START**

---
## Steps to follow to set up node.js and npm:

1. Download node js from [https://nodejs.org/en/](https://nodejs.org/en/).
2. Check the version using `node -v` and `npm -v`.

#### Initializing the json package:

1. Go to git-folder and type `npm init`.
2. this will create **package.json** file in the folder.

#### Installing npm modules

1. Install an NPM module, **lite-server**, that allows you to run a Node.js based development web server
2. `npm install lite-server --save-dev`
3. Add the following lines in the last section of package.json file `  "devDependencies": { "lite-server": "^2.2.2"}`
4. Run `npm start`

## Steps to follow after setting npm and node:


1. Clone the repository `git clone <repo address>`
2. Go to folder conFusion through terminal.
3. `ng serve` to start the server.


---
